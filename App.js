import React, { Component } from 'react';
import AppComponent from './app/app-component';

export default class App extends Component {
  render() {
    return (
      <AppComponent/>
    );
  }
}
