import eventsdb from '../../assets/eventsdb';

export default class EventsDataSource {
  get events() {
    const eventSections = groupSectionsBy(eventsdb, "month")
    const sortedSections = eventSections.sort((a, b) => Number(a.title) > Number(b.title));
    return sortedSections;
  }
}

function groupSectionsBy(list, props) {
  return list.reduce((a, b) => {
    let section = a.find(x => x.title === b[props]);
    if (typeof section !== 'undefined') {
      section.data.push(b)
    } else {
      a.push({title: b[props], data: [b]})
    }
    return a
  }, []);
}
