import { Platform } from 'react-native';

export const primaryColor = 'rgba(244,178,0,1.0)'
export const primaryFont = Platform.OS === 'ios' ? 'AvenirNextCondensed-Regular' : 'normal'

export function formatMonth(month) {
  const monthNames = ["Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"];
  return monthNames[month-1]
}
