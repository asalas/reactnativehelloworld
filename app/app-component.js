import React, { Component } from 'react';
import { SectionListBasics } from './event_list/event-list-view';
import { DetailsScreen } from './event_details/event-details-view';
import { createStackNavigator, createAppContainer } from 'react-navigation';

const AppNavigator = createStackNavigator(
  {
    Home: SectionListBasics,
    Details: DetailsScreen
  },
  {
    initialRouteName: "Home",
    headerBackTitleVisible: false
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
