import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { formatMonth } from '../common';
import { EventDetailsHeader, EventDetailsPlaceTime, EventDetailsLink, EventDetailsDescription } from './event-details-cells';

export class DetailsScreen extends React.Component {
  static navigationOptions = {
    headerTintColor: 'black'
  };

  renderItem = ({item, index}) => {
    switch(index) {
      case 0:
        return (<EventDetailsHeader text={item.text} />);
      case 1:
        return (<EventDetailsPlaceTime month={item.month} place={item.place} />);
      case 2:
        return (<EventDetailsLink text={item.text} />);
      default:
        return (<EventDetailsDescription text={item.text} />);
    }
  }

  render() {
    const { navigation } = this.props;
    const event = navigation.getParam('event');
    return (
      <FlatList
        data={makeEventFlatListData(event)}
        renderItem={this.renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}

function makeEventFlatListData(event) {
  return [
    {text: event.name},
    {month: formatMonth(event.month), place: event.place},
    {text: event.url},
    {text: event.description}
  ]
}
