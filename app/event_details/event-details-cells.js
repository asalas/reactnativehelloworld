import React, { Component } from 'react';
import { StyleSheet, View, Text, Linking, TouchableOpacity } from 'react-native';
import { primaryColor, primaryFont } from '../common';

export class EventDetailsHeader extends React.Component {
  render() {
    return (
      <View style={styles.yellowBackground} >
        <Text style={styles.eventHeader}>{this.props.text.toUpperCase()}</Text>
      </View>
    );
  }
}

export class EventDetailsPlaceTime extends React.Component {
  render() {
    return (
      <View style={[styles.eventDetailCell, {flex: 1, flexDirection: 'row', justifyContent: 'space-between'}]} >
        <View style={{flex: 1, alignItems: 'center'}} >
          <Text style={styles.placeMonthTitle}>Al voltant del</Text>
          <Text style={styles.placeMonth}>{this.props.month.toUpperCase()}</Text>
        </View>
        <View style={{flex: 1, alignItems: 'center'}} >
          <Text style={styles.placeMonthTitle}>A</Text>
          <Text style={styles.placeMonth}>{this.props.place}</Text>
        </View>
      </View>
    );
  }
}

export class EventDetailsLink extends React.Component {

  handleClick = () => {
    Linking.canOpenURL(this.props.text).then(supported => {
      if (supported) {
        Linking.openURL(this.props.text);
      } else {
        console.log("Don't know how to open URI: " + this.props.text);
      }
    });
  };

  render() {
    return (
      <TouchableOpacity onPress={this.handleClick}>
        <Text style={[styles.cellPadding, styles.link]}>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

export class EventDetailsDescription extends React.Component {
  render() {
    return (
      <Text style={[styles.cellPadding, styles.description]}>{this.props.text}</Text>
    );
  }
}

export const styles = StyleSheet.create({
  yellowBackground: {
    backgroundColor: primaryColor
  },
  eventHeader: {
    fontSize: 25,
    fontWeight: 'bold',
    fontFamily: primaryFont,
    alignSelf: 'center',
    paddingTop: 80,
    paddingBottom: 10,
    paddingLeft: 30,
    paddingRight: 30
  },
  eventDetailCell: {
    backgroundColor: 'white',
    padding: 10,
    fontSize: 18,
  },
  placeMonthTitle: {
    // fontFamily: primaryFont,
    fontSize: 15,
    color: 'gray'
  },
  placeMonth: {
    // fontFamily: primaryFont,
    fontSize: 20,
    fontWeight: 'bold',
  },
  cellPadding: {
    padding: 10,
  },
  description: {
    fontStyle: 'italic',
    fontSize: 17,
    color: 'dimgray'
  },
  link: {
    fontSize: 17,
    color: 'blue'
  }
})
