import React, { Component } from 'react';
import { Image, TouchableOpacity, StyleSheet, View, Text } from 'react-native';
import { primaryColor, primaryFont } from '../common';

export default class EventCell extends Component {
  render() {
    return (
      <TouchableOpacity onPress={() => this.props.navigation.push('Details', {
        event: this.props.item
      })}>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <View style={styles.icon} />
            <View style={styles.titleLocation}>
              <Text style={styles.title}>{this.props.item.name.toUpperCase()}</Text>
              <View style={styles.locationContainer}>
                <Image style={styles.locationImage} source={require('../../assets/locationIcon.png')} />
                <Text style={styles.place}>Barcelona</Text>
              </View>
            </View>
          </View>
          <Text numberOfLines={7} style={styles.description}>{removeLineBreaks(this.props.item.description)}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

function removeLineBreaks(description) {
  return description.replace(/(\r\n|\n|\r)/gm," ");
}

export const styles = StyleSheet.create({
  container: {
    paddingLeft: 20,
    paddingRight: 20
  },
  headerContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 10,
    backgroundColor: 'white',
  },
  icon: {
    width: 50,
    height: 50,
    backgroundColor: primaryColor,
    borderRadius: 3
  },
  titleLocation: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 10
  },
  locationContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontSize: 19,
    fontWeight: 'bold',
    fontFamily: primaryFont,
  },
  locationImage: {
    tintColor: 'darkgray',
    width: 15,
    height: 15,
  },
  place: {
    fontSize: 15,
    color: 'darkgray'
  },
  description: {
    fontStyle: 'italic',
    fontSize: 17,
    color: 'dimgray'
  }
})
