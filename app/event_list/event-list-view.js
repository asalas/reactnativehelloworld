import React, { Component } from 'react';
import { SectionList, View } from 'react-native';
import EventsDataSource from '../data_source/eventsDataSource';
import { primaryFont, formatMonth } from '../common';
import MonthHeader from './event-list-header'
import EventCell from './event-list-cell'

export class SectionListBasics extends Component {
  static navigationOptions = {
    title: 'NO ET PERDIS RES',
    headerTitleStyle: {
      fontFamily: primaryFont,
      fontSize: 20
    }
  };
  render() {
    return (
      <SectionList
        ItemSeparatorComponent={() => <EventListSeparator/> }
        sections={new EventsDataSource().events}
        renderItem={({item}) => <EventCell item={item} navigation={this.props.navigation} />}
        renderSectionHeader={({section}) => <MonthHeader month={formatMonth(section.title)} />}
        keyExtractor={(item, index) => index}
      />
    );
  }
}

class EventListSeparator extends Component {
  render() {
    return (
      <View style={{height: 0.5, width: '80%', alignSelf: 'center', backgroundColor: '#C8C8C8', marginTop: 20, marginBottom: 20}}/>
    );
  }
}
