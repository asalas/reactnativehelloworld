import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { primaryColor, primaryFont } from '../common';

export default class MonthHeader extends Component {
  render() {
    return (
      <View style={styles.container} >
        <Text style={styles.text}>{this.props.month.toUpperCase()}</Text>
        <View style={styles.yellowLine} />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
  },
  yellowLine: {
    backgroundColor: primaryColor,
    flex: 1,
    height: 2
  },
  text: {
    fontSize: 19,
    fontWeight: 'bold',
    fontFamily: primaryFont,
    color: primaryColor,
    paddingLeft: 30,
    paddingRight: 25,
  }
})
